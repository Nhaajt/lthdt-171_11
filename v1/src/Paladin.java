public class Paladin extends Knight {

    public Paladin(int baseHp, int wp) {
        super(baseHp, wp);
    }

    @Override
    public double getCombatScore() {
        int fibonacciIndex = Utility.Fibonacci.getIndex(getBaseHp());
        return fibonacciIndex == Utility.Fibonacci.NOT_FIBONACCI ?
                3 * getBaseHp() :
                1000 + fibonacciIndex;
    }
}
