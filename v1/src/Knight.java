public class Knight extends Fighter {

    public Knight(int baseHp, int wp) {
        super(baseHp, wp);
    }

    @Override
    public double getCombatScore() {
        return Utility.isSquare(Battle.GROUND) ?
                2 * getBaseHp() :
                getWp() == 1 ?
                        getBaseHp() :
                        getBaseHp() / 10.0;
    }
}
