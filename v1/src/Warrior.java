public class Warrior extends Fighter {

    public Warrior(int baseHp, int wp) {
        super(baseHp, wp);
    }

    @Override
    public double getCombatScore() {
        return Utility.isPrime(Battle.GROUND) ?
                2 * getBaseHp() :
                getWp() == 1 ?
                        getBaseHp() :
                        getBaseHp() / 10.0;
    }
}
