import java.util.ArrayList;
import java.util.HashMap;

public class Utility {
    public static boolean isPrime(long x) {
        if (x < 2) return false;
        for (long i = 2; i <= Math.sqrt(x); i++)
            if (x % i == 0) return false;
        return true;
    }

    public static boolean isSquare(long x) {
        if (x < 0) return false;
        double sqrt = Math.sqrt(x);
        return sqrt == (long) sqrt;
    }

    static class Fibonacci {
        public static final int NOT_FIBONACCI = -1;

        private static HashMap<Long, Integer> fibonacciMap = new HashMap<>();
        private static ArrayList<Long> fibonacciList = new ArrayList<Long>() {{
            add(-1l);
            add(-1l);
        }};
        private static long a = 0, b = 1;

        public static int getIndex(long num) {
            for (int a = 0, b = 1, index = 2; b <= num; index += 2) {
                if (num == (a += b)) return index;
                if (num == (b += a)) return index + 1;
            }
            return NOT_FIBONACCI;
        }

        @Deprecated
        public static int getIndex_UsingMap(int num) {
            for (int index = fibonacciMap.getOrDefault(b, 2); b <= num; ) {
                fibonacciMap.put(a += b, index++);
                fibonacciMap.put(b += a, index++);
            }
            return fibonacciMap.getOrDefault(num, NOT_FIBONACCI);
        }

        @Deprecated
        public static int getIndex_UsingList(int num) {
            for (int index = fibonacciList.size(); b <= num; index += 2) {
                if (fibonacciList.add(a += b) && num == a) return index;
                if (fibonacciList.add(b += a) && num == b) return index + 1;
            }
            return fibonacciList.indexOf(num);
        }

        public static long ofIndex(int index) {
            for (int a = 1, b = 2, i = 2; i <= index; b += a += b) {
                if (index == i++) return a;
                if (index == i++) return b;
            }
            return NOT_FIBONACCI;
        }

        @Deprecated
        public static long ofIndex_UsingList(int index) {
            while (fibonacciList.size() <= index) {
                fibonacciList.add(a += b);
                fibonacciList.add(b += a);
            }
            try {
                return fibonacciList.get(index);
            } catch (IndexOutOfBoundsException e) {
                return NOT_FIBONACCI;
            }
        }

        public static boolean isFibonacci(int num) {
            return getIndex(num) != NOT_FIBONACCI;
        }
    }
}
