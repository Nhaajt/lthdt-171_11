public class TestUtility {
    public static void main(String[] args) {
//        testFibonacciOfIndex(20);
        testGetFibonacciIndex(10000000);
    }

    private static void testFibonacciOfIndex(int iteration) {
        TimedTest fibonacciOfIndex = new TimedTest() {
            @Override
            public void test() {
                for (int i = iteration; i >= -5; i--) {
                    long f = Utility.Fibonacci.ofIndex(i);
//                    System.out.println(i + " : " + f);
                }
            }
        };
        System.out.println(fibonacciOfIndex.run(10));
    }

    private static void testGetFibonacciIndex(int iteration) {
        TimedTest oldGetFibonacciIndex = new TimedTest() {
            @Override
            public void test() {
                for (int i = iteration; i >= 0; i--) {
                    int f = Utility.Fibonacci.getIndex(i);
//                    if (f != Utility.Fibonacci.NOT_FIBONACCI)
//                        System.out.println(i + " : " + f);
                }
            }
        };

        System.out.println(oldGetFibonacciIndex.run(10));
    }
}
