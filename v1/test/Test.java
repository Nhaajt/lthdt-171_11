public class Test {
    public static void main(String[] args) {
        System.out.println("Battle Begin.");
        Battle.moveToRandomGround();

        Combatable[] knights = TeamMaker.makeTeam1();
        System.out.println("Team 1");
        for (Combatable knight : knights)
            System.out.println("\t" + knight);

        Combatable[] warriors = TeamMaker.makeTeam2();
        System.out.println("Team 2");
        for (Combatable warrior : warriors)
            System.out.println("\t" + warrior);

        TestBattle battle = new TestBattle(knights, warriors);
        battle.combat();

        System.out.println("Battle End.");
    }
}
