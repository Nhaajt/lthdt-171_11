public abstract class TimedTest implements Comparable<TimedTest> {
    public enum TimeUnit {
        NANOSECOND(1.0),
        MICROSECOND(NANOSECOND.value * 1000),
        MILLISECOND(MICROSECOND.value * 1000),
        SECOND(MILLISECOND.value * 1000);

        double value;

        TimeUnit(double value) {
            this.value = value;
        }
    }

    private TimeUnit timeUnit = TimeUnit.MILLISECOND;
    private double lastRunTime = -1;

    public TimedTest() {
    }

    public TimedTest(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

    public abstract void test();

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public TimedTest setTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
        return this;
    }

    public double getLastRunTime() {
        return lastRunTime == -1 ? run().getLastRunTime() : lastRunTime;
    }

    public TimedTest run(int repeat) {
        long startTime = System.nanoTime();
        for (int i = 0; i < repeat; i++) test();
        lastRunTime = (System.nanoTime() - startTime)
                / timeUnit.value
                / repeat;
        return this;
    }

    public TimedTest run() {
        return run(10);
    }

    @Override
    public int compareTo(TimedTest otherTest) {
        return (int) (getLastRunTime() - otherTest.getLastRunTime());
    }

    @Override
    public String toString() {
        return String.format( "Run time: %.4f %s",
                getLastRunTime(), timeUnit);
    }
}
