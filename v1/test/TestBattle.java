import java.util.Random;

public class TestBattle {
    private static final double RATE_WIN = 0.5D;
    private static final int GROUND_BOUND = 999;
    public static int GROUND = 1;

    private Combatable[] mTeam1;
    private Combatable[] mTeam2;

    public static void moveToRandomGround() {
        Random rand = new Random();
        GROUND = rand.nextInt(999) + 1;
        System.out.println("Moving to ground " + GROUND + ".");
    }

    public TestBattle(Combatable[] team1, Combatable[] team2) {
        this.mTeam1 = team1;
        this.mTeam2 = team2;
    }

    public void combat() {
        double pr = 0.0D;

        for (int i = 0; i < this.mTeam1.length; ++i) {
            double result = this.duel(this.mTeam1[i], this.mTeam2[i]);
            pr += result;

            System.out.printf("\n%s vs %s\n", mTeam1[i], mTeam2[i]);
            System.out.println("pR[" + i + "] = " + result);

            if (i == 0 && result >= 0.5D) moveToRandomGround();
        }

        pr /= (double) this.mTeam1.length;
        System.out.println("\nBattle result: pR = " + pr);
    }

    private double duel(Combatable cb1, Combatable cb2) {
        double score1 = cb1.getCombatScore();
        double score2 = cb2.getCombatScore();
        double pr = (score1 - score2 + 999.0D) / 2000.0D;
        return pr;
    }
}
